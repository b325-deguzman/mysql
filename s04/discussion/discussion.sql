-- Add 5 artists;
INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Add album to TS:
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-1-1", 3);

INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("Fearless", 246, "Pop rock", 102);
INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("Love Story", 213, "Country pop", 102);


INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-1-1", 3);

INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("State of Grace", 253, "Rock, alternative rock, arena rock", 103);
INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("Red", 204, "Country", 103);

-- LADY GAGA
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-1-1", 4);

-- a star is born
INSERT INTO songs (song_title, song_length, song_genre, album_id)  VALUES ("Black Eyes", 151, "Rock and roll", 104);
INSERT INTO songs (song_title, song_length, song_genre, album_id)  VALUES ("Shallow", 201, "Country, rock, folk rock", 104);

-- Lady Gaga
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-1-1", 4);

-- [SECTION] ADVANCES Selects:

-- exclude record / not equal to
SELECT * FROM songs WHERE id != 11;

SELECT song_name, genre * FROM songs WHERE id != 11;

-- Greater than or equal operator:
SELECT * FROM songs WHERE id >= 11;

-- Less than or equal operator:
SELECT * FROM songs WHERE id <= 11;

-- What if I want those records with id that has 5 to 11:
SELECT * FROM songs WHERE id >= 5 AND id <= 11; 

SELECT * FROM songs WHERE id BETWEEN 5 AND 11;

-- GET specific IDs:
SELECT * FROM songs WHERE id = 11;

-- OR Operator:
SELECT * FROM songs WHERE id = 1 OR id = 11 OR id = 5;

-- IN Operator / same as OR operator:
SELECT * FROM songs WHERE id IN (1, 11, 5);

-- Find Partial matches:
-- all song name ENDING in a
SELECT * FROM songs WHERE song_name LIKE "%a";

-- all song name STARTING in a
SELECT * FROM songs WHERE song_name LIKE "a%";

-- all song name with a in the MIDDLE
SELECT * FROM songs WHERE song_name LIKE "%a%";

SELECT * FROM songs WHERE album_id LIKE "10_";

-- SORT record
-- Ascending alphabetically
SELECT * FROM songs ORDER BY song_name ASC;

-- Descending 
SELECT * FROM songs ORDER BY song_name DESC;

-- DISTINCT Records:
SELECT DISTINCT genre FROM songs;
SELECT DISTINCT album_id FROM songs;

-- [SECTION] Table joins:
-- SELECT * FROM table JOIN table ON primary key = foreign key;
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

-- JOIN albums table and songs table:
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id;

SELECT albums.album_title, 
	songs.song_name 
FROM albums 
	JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title, songs.song_name FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

SELECT 
	artists.name,
	artists.id,
	albums.artist_id,
	albums.album_title,
	albums.id,
	songs.album_id, 
	songs.song_name 
FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

SELECT
	artists.name,
	albums.album_title,
	albums.id,
	songs.song_name
FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id
	LEFT JOIN songs ON albums.id = songs.album_id;