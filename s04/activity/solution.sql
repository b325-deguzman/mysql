SELECT * FROM artists WHERE name like '%d%';

SELECT * FROM songs WHERE length < 350;

SELECT albums.album_title,
	songs.song_name,
	songs.length
FROM albums
	INNER JOIN songs ON albums.id = songs.album_id;

SELECT artists.name,
	albums.album_title
FROM artists
	JOIN albums ON artists.artist_id = albums.artist_id
WHERE albums.album_title LIKE '%a%';

SELECT DISTINCT albums.album_title
FROM albums
ORDER BY album_title DESC
LIMIT 4;

SELECT *
FROM albums
	JOIN songs on albums.id = songs.album_id
ORDER BY album_title DESC;