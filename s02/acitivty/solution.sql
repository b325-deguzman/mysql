CREATE DATABASE blog_db;
USE blog_db;

CREATE TABLE users(
    id int NOT NULL AUTO_INCREMENT,
    email varchar(100) NOT NULL,
    password varchar(300) NOT NULL,
    datetime_created datetime NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE posts(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title varchar(500) NOT NULL,
    content varchar(5000) NOT NULL,
    datetime_posted datetime NOT NULL,
    author_id int NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT  FK_author_id
    FOREIGN KEY (author_id) REFERENCES users(id) ON UPDATE cascade ON DELETE restrict
);

CREATE TABLE post_comments(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    content varchar(5000) NOT NULL,
    datetime_commented datetime NOT NULL,
    post_id int NOT NULL,
    user_id int NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT FK_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id),
    CONSTRAINT FK_user_id
    FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE cascade ON DELETE restrict
);

CREATE TABLE post_likes(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    post_id int NOT NULL,
    user_id int NOT NULL,
    datetime_liked datetime NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT FK_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE cascade ON DELETE restrict,
    CONSTRAINT FK_user_id
    FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE cascade ON DELETE restrict
);