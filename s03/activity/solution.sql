

INSERT INTO users(email, password)
	VALUES ('johnsmith@gmail.com', "passwordA");

INSERT INTO users(email, password)
	VALUES ('juandelacruz@gmail.com', "passwordB");

INSERT INTO users(email, password)
	VALUES ('janesmith@gmail.com', "passwordC");

INSERT INTO users(email, password)
	VALUES ('mariadelacruz@gmail.com', "passwordD");

INSERT INTO users(email, password)
	VALUES ('johndoe@gmail.com', "passwordE");

INSERT INTO posts(title, content, author_id)
	VALUES ('First Code', "Hello World!", 1);

INSERT INTO posts(title, content, author_id)
	VALUES ('Second Code', "Hello Earth!", 1);

INSERT INTO posts(title, content, author_id)
	VALUES ('Third Code', "Welcome to Mars!", 1);

INSERT INTO posts(title, content, author_id)
	VALUES ('Fourth Code', "Bye bye solar sytem!", 1);

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!";

DELETE FROM users WHERE email = 'johndoe@gmail.com';